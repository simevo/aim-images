# Build container images

Helper repo to build container images on Gitlab CI.

## How to test the image build locally

```sh
for IMAGE in jh-base
do
  docker build "$IMAGE"
done
```

## License

Copyright (C) 2024 [simevo s.r.l.](https://simevo.com) for [Agenzia Italia Meteo](https://www.agenziaitaliameteo.it/).

Licensed under the [BSD 3-Clause (`BSD-3-Clause`)](LICENSE) as per [linee guida per l’Acquisizione e il riuso di software per la Pubblica Amministrazione](https://docs.italia.it/italia/developers-italia/lg-acquisizione-e-riuso-software-per-pa-docs/it/stabile/riuso-software/licenze-aperte-e-scelta-di-una-licenza.html#scelta-di-una-licenza).
